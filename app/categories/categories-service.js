/* global _ */
/* global angular */

(function (ng) {
  ng.module('app')
    .service('categoryService', categoryService);

  categoryService.$inject = ['$q', '$http', '$state'];

  function categoryService($q, $http, $state) {
    var categoryService = {};

    // get categories list
    categoryService.list = function list() {
      return $http.get('http://localhost:3000/categories/')
      .then(function (response) {
        return response.data;
      });
    };

    // save category
    categoryService.save = function save(category) {
      if (!category._id) {
        return this.add(category);
      }
      return this.update(category);
    };

    // add new category
    categoryService.add = function add(category) {
      category.isEdditing = false;
      return $http.post('http://localhost:3000/categories/', category)
      .then(function (response) {
        return response.data;
      });
    };

    // update category value
    categoryService.update = function update(category) {
      var updatingCategory =  _.omit(category, ['_id']);
      return $http.put('http://localhost:3000/categories/' + category._id, updatingCategory)
      .then(function (response) {
        return response.data;
      });
    };

    // delete category
    categoryService.remove = function remove(category) {
      return $http.delete('http://localhost:3000/categories/' + category._id)
      .then(function () {
        if ($state.params.categoryId === category._id) {
          $state.go('categoryProducts', { categoryId: '0' });
        }
        return category._id;
      });
    };

    return categoryService;
  }
})(angular);
