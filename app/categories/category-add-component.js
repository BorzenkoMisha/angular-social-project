

/* global _ */
/* global angular */

(function(ng){

  ng.module('app')
    .directive('categoryAdd', categoryAdd);

  function categoryAdd() {
    return {
      restrict: 'E',
      controller: 'categoryAddController',
      controllerAs: '$ctrlForm',
      bindToController: true,
      templateUrl: 'app/categories/category-add.html',
      require: {
        categoriesController: '^^categories'
      }
    };
  }
})(angular);
