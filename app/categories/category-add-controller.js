

/* global _ */
/* global angular */

(function(ng) {

  ng.module('app')
    .controller('categoryAddController', categoryAddController);

  function categoryAddController() {}

  // Initial state
  categoryAddController.prototype.$onInit = function onInit() {
    this.category = {
      title: ''
    };
  };

  // Toggle add btn showing
  categoryAddController.prototype.$toggleAdd = function $toggAdd() {
    this.$isAdding = !this.$isAdding;
  };

  // Add new category
  categoryAddController.prototype.add = function add() {
    this.categoriesController.add(this.category).then(function(){
      this.$toggleAdd();
      this.$onInit();
    }.bind(this));
  };
})(angular);
