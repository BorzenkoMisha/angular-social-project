/* global _ */
/* global angular */

(function (ng) {
  ng.module('app')
    .controller('categoriesController', categoriesController);

  categoriesController.$inject = ['$scope', 'categoryService', 'productsService'];

  function categoriesController($scope, categoryService, productsService) {
    var $ctrl = this;
    $ctrl.category = '';

    // Services
    $ctrl.productsService = productsService;
    $ctrl.categoryService = categoryService;

    this.list().then(function (result) {
      $ctrl.categories = result;
    });
  }

  // Get category list
  categoriesController.prototype.list = function () {
    return this.categoryService.list();
  };

  // Add new category
  categoriesController.prototype.add = function save(category) {
    var $ctrl = this;
    return $ctrl.categoryService.save(category).then(function (response) {
      $ctrl.categories.push(response);
    });
  };

  // Edit category
  categoriesController.prototype.edit = function edit(category) {
    var $ctrl = this;
    if (category.isEdditing) {
      $ctrl.categoryService.save(category)
        .then(function () {
          category.isEdditing = !category.isEdditing;
        });
    } else {
      category.isEdditing = !category.isEdditing;
    }
  };

  // Delete category
  categoriesController.prototype.remove = function remove(item) {
    var $ctrl = this;
    var result = confirm('Want to delete?');
    if (result) {
      $ctrl.categoryService.remove(item).then(function () {
        var index = $ctrl.categories.indexOf(item);
        $ctrl.categories.splice(index, 1);
      });
    }
  };
})(angular);
