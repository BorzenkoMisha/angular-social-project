/* global _ */
/* global angular */

(function (ng) {
  ng.module('app')
    .directive('categories', categoriesComponent);

  function categoriesComponent() {
    return {
      restrict: 'E',
      controller: 'categoriesController',
      controllerAs: '$ctrl',
      scope: {},
      templateUrl: 'app/categories/categories.html',
      bindToController: true
    };
  }
})(angular);
