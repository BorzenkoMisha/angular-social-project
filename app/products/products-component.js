/* global _ */
/* global angular */

(function(ng){

  ng.module('app')
    .directive('products', productsComponent);

  function productsComponent() {
    return {
      restrict: 'E',
      controller: 'productsController',
      scope: {},
      templateUrl: 'app/products/products.html',
      controllerAs: '$ctrl',
      bindToController: true
    };
  }
})(angular);
