/* global _ */
/* global angular */

(function (ng) {
  ng.module('app')
    .controller('productsController', productsController);

  productsController.$inject = ['$scope', '$uibModal', 'productsService', '$stateParams'];

  function productsController($scope, $uibModal, productsService, $stateParams) {
    var $ctrl = this;
    $ctrl.favoriteRoute = false;

    $ctrl.selectedCategoryId = $stateParams.categoryId;
    $ctrl.productsService = productsService;
    if ($ctrl.selectedCategoryId === 'favorites') {
      $ctrl.favoriteRoute = true;
    }
    // Init modal
    $ctrl.show = function show(data) {
      var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'app/products/product-modal.html',
        controller: 'modalController',
        size: 'lg',
        resolve: {
          data: function() {
            return data;
          }
        }
      });

      // Popup response
      modalInstance.result.then(function (product) {
        // Add new product
        if (!product._id) {
          productsService.add(product).then(function (product) {
            $ctrl.productsService.list($ctrl.selectedCategoryId).then(function (result) {
              $ctrl.products = result;
            });
          });
        } else {
          // Change available
          productsService.save(product).then(function (data) {
            $ctrl.productsService.list($ctrl.selectedCategoryId).then(function (result) {
              $ctrl.products = result;
            });
          });
        }
      });
    };

    // Take filtered products List
    $ctrl.productsService.list($ctrl.selectedCategoryId).then(function (result) {
      $ctrl.products = result;
    });
  }

  // Remove product
  productsController.prototype.remove = function remove(product, index) {
    var $ctrl = this;
    var result = confirm('Want to delete?');
    if (result) {
      $ctrl.productsService.remove(product).then(function (data) {
        $ctrl.products.splice(index, 1);
      });
    }
  };

  // Show popup with new product
  productsController.prototype.add = function add() {
    this.show(this.selectedCategoryId);
  };

  // Toggle favorite state
  productsController.prototype.toggleFavorite = function toggleFavorite(product) {
    product.isFavorite = !product.isFavorite;
    this.productsService.save(product);
  };
})(angular);
