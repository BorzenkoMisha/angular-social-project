/* global _ */
/* global angular */

(function(ng) {

  ng.module('app')
    .service('productsService', productsService);

  productsService.$inject = ['$q', '$http'];

  function productsService($q, $http) {
    var productsService = {};
    var data = [];

    // get products
    productsService.list = function list(id) {
      if (id) {
        return this.showFiltered(id);
      }
      return $http.get('http://localhost:3000/products/')
      .then(function(response) {
        return response.data;
      });
    };

    // remove product
    productsService.remove = function remove(product) {
      return $http.delete('http://localhost:3000/products/' + product._id)
      .then(function(response) {
        return response.data;
      });
    };

    // save product
    productsService.save = function save(product) {
      if (product._id) {
        return this.update(product);
      }
      return this.add(product);
    };

    // add new product
    productsService.add = function add(product) {
      return $http.post('http://localhost:3000/products/', product)
      .then(function(response) {
        return response.data;
      });
    };

    // update current
    productsService.update = function update(product) {
      var updatingProduct =  _.omit(product, ['_id']);
      return $http.put('http://localhost:3000/products/' + product._id, updatingProduct)
      .then(function(response) {
        return response.data;
      });
    };

    // filter products
    productsService.showFiltered = function showFiltered(id) {
      return $http.get('http://localhost:3000/products?category_id=' + id)
      .then(function(response) {
        return response.data;
      });
    };

    return productsService;
  }
})(angular);
