/* global _ */
/* global angular */

(function(ng) {

  ng.module('app')
    .controller('modalController', modalController);

  modalController.$inject = ['$scope', '$uibModalInstance', 'categoryService', 'objectsService', 'data'];

  function modalController($scope, $uibModalInstance, categoryService, objectsService, data) {
    $scope.product = {};

    // take categories list for product
    categoryService.list().then(function(result) {
      $scope.categories = result;
      // Check data is product or category id
      if (typeof(data) === 'object') {
        $scope.product = objectsService.cloneObject(data);
        $scope.product.category_id = data.category_id.toString();
      } else {
        $scope.product.category_id = data;
      }
    });

    // Submit popup
    $scope.submit = function submit(product) {
      $uibModalInstance.close(product);
    };

    // Close popup
    $scope.close = function close() {
      $uibModalInstance.dismiss('cancel');
    };

    // Check url validation
    $scope.validateURL = function validateURL(url) {
      var urlregex = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
      return urlregex.test(url);
    };

    // Check url type
    $scope.checkUrlType = function checkUrlType(url) {
      var parser = document.createElement('a');
      if (url && $scope.validateURL(url)) {
        parser.href = url;

        // Check for youtube
        if (parser.hostname === 'www.youtube.com' || parser.hostname === 'youtube.com') {
          $scope.product.type = 'youtube';

          // Check for image
        } else if (url.match(/\.(jpeg|jpg|gif|png)$/) !== null) {
          $scope.product.type = 'image';

          // Default value(text)
        } else {
          $scope.product.type = 'text';
        }
      } else {
        $scope.product.type = null;
      }
    };
  }
})(angular);
