/* global angular */
(function(ng){

  ng.module('app', ['ui.router', 'youtube-embed', 'ui.bootstrap']).config(Config);

  Config.$inject = ['$stateProvider', '$urlRouterProvider'];
  function Config($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/category/0/');
    $stateProvider
      .state('categoryProducts', {
        url: '/category/:categoryId/',
        template: '<products></products>'
      })
      .state('favoritesProducts', {
        url: '/category/favorites/',
        template: '<products></products>'
      });
  }
})(angular);
