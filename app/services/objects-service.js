(function(ng){


  ng.module('app')
    .service('objectsService', objectsService);

  function objectsService(){

    var objectsService = {};

    objectsService.cloneObject = function(source){
      if (Object.prototype.toString.call(source) === '[object Array]') {
        var clone = [];
        for (var i=0; i<source.length; i++) {
          clone[i] = objectsService.cloneObject(source[i]);
        }
        return clone;
      } else if (typeof(source)=="object") {
        var clone = {};
        for (var prop in source) {
          if (source.hasOwnProperty(prop)) {
            clone[prop] = objectsService.cloneObject(source[prop]);
          }
        }
        return clone;
      } else {
        return source;
      }
    };

    return objectsService;
  }

})(angular);
