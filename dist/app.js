(function(){

  config.$inject = ['$stateProvider', '$urlRouterProvider'];
  angular.module('app', ['ui.router','youtube-embed','ui.bootstrap']).config(config);

  "ngInject";
  function config($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/category/0/');
    $stateProvider
      .state('categoryProducts', {
          url: '/category/:categoryId/',
          template: '<products></products>'
      })
      .state('favoritesProducts', {
        url: '/category/favorites/',
        template: '<products></products>'
      })
  }
})();
